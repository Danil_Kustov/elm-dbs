apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "integrations.name" . }}
  labels:
    {{- include "integrations.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "integrations.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        {{- include "integrations.selectorLabels" . | nindent 8 }}
      annotations:
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/integrations/service:{{ .Values.images.service }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: AMO_CRM_CLIENT_ID
              value: {{ .Values.appconfig.AmoCrmClientID }}
            - name: AMO_CRM_CLIENT_SECRET
              value: {{ .Values.appconfig.AmoCrmClientSecret }}
            - name: AMO_CRM_SCOPES
              value: {{ join "," .Values.appconfig.AmoCrmScopes | quote }}
            - name: AMO_CRM_AUTH_URL
              value: {{ .Values.appconfig.AmoCrmAuthURL }}
            - name: AMO_CRM_REDIRECT_URL
              value: {{ .Values.appconfig.AmoCrmRedirectURL }}
            - name: SAML_INSECURE_FETCH_METADATA
              value: {{ .Values.appconfig.SamlInsecureFetchMetadata | quote }}
            - name: ELMA365_AVAILABLE_INTEGRATIONS
              value: {{ join "," .Values.appconfig.availableIntegrations | quote }}
            - name: ELMA365_SKIP_SSL_VERIFY
              value: {{ .Values.global.skipSslVerify | quote }}
            - name: LDAP_TIMEOUT
              value: {{ .Values.appconfig.ldapTimeout | quote }}
            - name: LDAP_JOB_TTL
              value: {{ .Values.appconfig.ldapJobTTL | quote }}
            - name: LDAP_JOB_DELAY_BETWEEN_ATTEMPTS
              value: {{ .Values.appconfig.ldapJobDelayBetweenAttempts | quote }}
            - name: ELMA365_R7_INSTALL_URL
              value: {{ default "https://store.elma365.ru/uploads/packages/ext_00e82811_87d6_4d46_a5d4_e444ed8a857a/2c617b4038363620892417a87a53d02fa8e795a1.qbpm" .Values.appconfig.r7InstallUrl | quote }}
          envFrom:
            - configMapRef:
                name: {{ include "integrations.name" . }}
            - secretRef:
                name: {{ include "integrations.name" . }}
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
        {{- if .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 12 }}
        {{- else if .Values.global.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.global.livenessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 12 }}
        {{- else if .Values.global.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.global.readinessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.startupProbe }}
          startupProbe:
          {{- toYaml .Values.startupProbe | nindent 12 }}
        {{- else if .Values.global.startupProbe }}
          startupProbe:
          {{- toYaml .Values.global.startupProbe | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
      {{- if eq .Values.global.solution "onPremise" }}
      {{- if .Values.global.ingress }}
      {{- if .Values.global.ingress.onpremiseTls }}
      {{- if .Values.global.ingress.onpremiseTls.enabledCA }}
          volumeMounts:
            - name: elma365-onpremise-ca
              subPath: elma365-onpremise-ca.pem
              mountPath: /etc/ssl/certs/elma365-onpremise-ca.pem
              readOnly: false
      volumes:
        - name: elma365-onpremise-ca
          configMap:
            name: {{ .Values.global.ingress.onpremiseTls.configCA }}
      {{- end }}
      {{- end }}
      {{- end }}
      {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "integrations.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
