apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "templater.name" . }}
  labels:
    {{- include "templater.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "templater.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        {{- include "templater.selectorLabels" . | nindent 8 }}
      annotations:
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:      
        - name: dottemplater
          image: "{{ .Values.global.image.repository }}/elma365/templater/dottemplater:{{ .Values.images.dottemplater }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          env:
            - name: ELMA365_LOG_LEVEL
              value: "ERROR"
            - name: ELMA365_LOGGING__LOGLEVEL__DEFAULT
              value: {{ .Values.logging.default }}
            - name: ELMA365_LOGGING__LOGLEVEL__MICROSOFT
              value: {{ .Values.logging.microsoft }}
            - name: ELMA365_LOGGING__LOGLEVEL__MICROSOFT.HOSTING.LIFETIME
              value: {{ .Values.logging.default }}
            - name: ELMA365_ALLOWEDHOSTS
              value: '*'
            - name: ELMA365_SKIP_SSL_VERIFY
              value: {{ .Values.global.skipSslVerify | quote }}
          ports:
          - name: http-dot
            containerPort: {{ .Values.global.http_port_dot }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
          volumeMounts:
            - name: barcode
              mountPath: /tmp/barcode 
          livenessProbe:
            httpGet:
              path: /ping
              port: {{ .Values.global.http_port_dot }}
            initialDelaySeconds: 30
            periodSeconds: 15
            timeoutSeconds: 2
            failureThreshold: 1
            successThreshold: 1
          readinessProbe:
            httpGet:
              path: /ping
              port: {{ .Values.global.http_port_dot }}
            initialDelaySeconds: 30
            periodSeconds: 15
            timeoutSeconds: 2
            failureThreshold: 1
            successThreshold: 1
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.dottemplater.resources) 0 -}}
          {{- toYaml .Values.dottemplater.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}         
        {{- end }}   
        {{- end }}   
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/templater/gateway:{{ .Values.images.gateway }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: ELMA365_RABBITMQ_USER
              value: {{ template "templater.rmquser" . }}
            - name: ELMA365_RABBITMQ_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: templater
                  key: rabbitmq_password
            - name: ELMA365_SKIP_SSL_VERIFY
              value: {{ .Values.global.skipSslVerify | quote }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
          volumeMounts:
            - name: barcode
              mountPath: /tmp/barcode     
          livenessProbe:
            httpGet:
              path: /healthz
              port: {{ .Values.global.http_port }}
            initialDelaySeconds: 30
            periodSeconds: 15
            timeoutSeconds: 2
            failureThreshold: 1
            successThreshold: 1
          readinessProbe:
            httpGet:
              path: /ready
              port: {{ .Values.global.http_port }}
            initialDelaySeconds: 30
            periodSeconds: 15
            timeoutSeconds: 2
            failureThreshold: 1
            successThreshold: 1
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}           
      volumes:
        - name: barcode
          emptyDir: {}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "templater.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
