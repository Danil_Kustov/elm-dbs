{{- $releaseName := .Release.Name }}
{{- $name := include "worker.name" . }}
{{- $gatewayName := include "gateway.name" . }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "worker.name" . }}
  labels:
    {{- include "worker.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "worker.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      labels:
        {{- include "worker.selectorLabels" . | nindent 8 }}
      annotations:
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
    spec:
      imagePullSecrets:
      {{- range .Values.global.image.pullSecret }}
      - name: {{ . }}
      {{- end }}
      {{- if ne (toString .Values.global.openshift) "true" }}
      initContainers:
      - name: vm-overcommit-memory
        image: "{{ default "docker.io" .Values.global.image.dockerRegistry }}/library/busybox:1.36"
        imagePullPolicy: "IfNotPresent"
        command: ["sysctl", "-w", "vm.overcommit_memory=1"]
        securityContext:
          privileged: true
      {{- end }}
      containers:
      - name: worker
        image: "{{ .Values.global.image.repository }}/elma365/serv_worker/worker:{{ .Values.images.worker }}"
        imagePullPolicy: {{ .Values.global.image.pullPolicy }}
        ports:
        - name: http-port-wrk
          containerPort: {{ .Values.service.http_port_wrk }}
        env:
        - name: HOST_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        - name: ELMA365_WORKER_RABBITMQ_USER
          value: {{ template "worker.rmquser" . }}
        - name: ELMA365_WORKER_RABBITMQ_PASSWORD
          valueFrom:
            secretKeyRef:
              name: worker
              key: rabbitmq_password
        {{- if .Values.appconfig.concurrency }}
        - name: ELMA365_WORKER_CONCURENCY
          value: {{ int .Values.appconfig.concurrency | quote }}
        {{- end }}
        - name: WORKER_HTTP_PORT
          value: "{{ .Values.service.http_port_wrk }}"
        {{ if .Values.global.skipSslVerify }}
        - name: ELMA365_SKIP_SSL_VERIFY
          value: {{ default "false" .Values.global.skipSslVerify | quote }}
        - name: NODE_TLS_REJECT_UNAUTHORIZED
          value: "0"
        {{- end }}
        {{- if .Values.appconfig.httpProxyAddr }}
        - name: HTTP_PROXY
          value: {{ .Values.appconfig.httpProxyAddr }}
        {{- end }}
        - name: ELMA365_GATEWAY_HOST
          value: {{ $gatewayName | quote }}
        - name: ELMA365_GATEWAY_PORT
          value: {{ printf ":%d" (int .Values.global.http_port) | quote }}
        - name: ELMA365_FORK_AVAILABLE_MEMORY_SIZE
          value: {{ int .Values.appconfig.forkAvailableMemorySize | quote }}
        - name: ELMA365_SKIP_COMPILATION_LIMITS
          value: {{ default "false" .Values.appconfig.skipCompilationLimits | quote }}
        envFrom:
        - configMapRef:
            name: elma365-env-config
            optional: true
        - secretRef:
            name: elma365-db-connections
            optional: true
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
        resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 10 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 10 -}}
          {{- end }}
        {{- end }}
        {{- end }}
      {{- if eq .Values.global.solution "onPremise" }}
      {{- if .Values.global.ingress }}
      {{- if .Values.global.ingress.onpremiseTls }}
      {{- if .Values.global.ingress.onpremiseTls.enabledCA }}
        volumeMounts:
          - name: elma365-onpremise-ca
            subPath: elma365-onpremise-ca.pem
            mountPath: /etc/ssl/certs/elma365-onpremise-ca.pem
            readOnly: false
      volumes:
        - name: elma365-onpremise-ca
          configMap:
            name: {{ .Values.global.ingress.onpremiseTls.configCA }}
      {{- end }}
      {{- end }}
      {{- end }}
      {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "worker.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
